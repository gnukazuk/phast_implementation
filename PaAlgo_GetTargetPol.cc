#include <cassert>
#include "PaAlgo.h"
#include "PaMetaDB.h"

/*!
  \file PaAlgo_NMR_coil.cc
  \brief You can access the info of position of NMR coils in 2015
  \date 09/Aug./2016
  \author G. Nukazuka
*/

/*!
  \brief return z coordinate of NMR coil
  \param run a number of this run
  \param coil_index index of coil
  \details coil index should be from 1 to 10
*/
Double_t PaAlgo::GetZOfNMRCoil( Int_t run , Int_t coil_index_arg )
{

  assert( coil_index_arg >= 1 && coil_index_arg <= 10);

  // only this function treats index of coils as from 0 to 9
  Int_t coil_index = coil_index_arg - 1;  
  Double_t pos[10] = { -9999.9 };

  if( run > 256000 && run < 264860 ) // DY2015
    {
      pos[0] = -292.0;      pos[1] = -281.0;      pos[2] = -267.0;
      pos[3] = -253.5;      pos[4] = -242.5;      pos[5] = -216.5;
      pos[6] = -206.0;      pos[7] = -191.5;      pos[8] = -178.0;
      pos[9] = -167.0;
    }
  else
    {
      cout << "PaAlgo::GetZOfNMRCoil( Int_t year , Int_t coil_index ) ==> NMR coil location for the run " << run
	   << " is not known. The available year is run in 2015" << endl;
      exit(1);
    }

  return pos[coil_index];
}

/*!
  \brief return the index of the closest NMR coil to z
  \param run a number of this run
  \param z the position that you want to check
  \details the index of coil is from 1 to 10
*/
Int_t PaAlgo::GetClosestNMRCoil( Int_t run, Double_t z )
{

  // this event should be taken in 2015
  assert( run > 256000 && run < 264860 );

  Double_t down_of_upstream, up_of_downstream, dtemp;
  Bool_t bl_target_location = GetTargetLocation( run, 
					       dtemp, dtemp, dtemp , down_of_upstream,
					       dtemp, dtemp, up_of_downstream , dtemp, 
					       dtemp, dtemp );

  assert( bl_target_location == true );
  
  // container of the difference b/w coil and given z
  // initialized with very big value
  Double_t diff = 1e5;

  // number of coil, should be 1 - 10
  Int_t coil_num = -1;

  // loop over coils from #1 to #10
  for( Int_t i=1; i<11; i++ )
    {

      Double_t z_coil = GetZOfNMRCoil( run, i ) ;
      // if this difference b/w coil and given z is smaller than stored difference, take it !
      if( diff > fabs(z - z_coil) )
	{
	  diff = z - z_coil;
	  coil_num = i;
	}
    }

  // if the closest coil is 5 (6) but the closest cell is downstream (upstream) cell,
  // change the closest coil to 6(5)
  // because center of target cells and b/w coil #5 & #6 are not same
  if( coil_num == 5 &&
      (fabs(down_of_upstream - z) > fabs(up_of_downstream - z )) )
    coil_num = 6;
  else  if( coil_num == 6 &&
      (fabs(down_of_upstream - z) < fabs(up_of_downstream - z )) )
    coil_num = 5;

    return coil_num;
}

/*!
  \brief return the index of the 2nd closest NMR coil to z
  \param run a number of this run
  \param z the position that you want to check
  \details the index of coil is from 1 to 10
 In some cased, -1 will be returned because the closest coil is enough to determin the polarization.
 Consider following situation:

 \code
 coil       1   2   3   4   5                6   7   8   9   10
 position   |   |   |   |   |                |   |   |   |   |  
          ---------------------            ---------------------
         |                     |          |                     |
         |A       B           C|     D    |                    E|
         |                     |          |                     |
          ---------------------            ---------------------
    
              upstream cell                   downstream cell
     -------------------------------------------------------------> z
 \endcode

 - case 1 : z = A, eg. given z is upstream of coil #1
   - closest coil: 1, 2nd closest coil: -1

 - case 2 : z = B, eg. #2 < z < #3
   - closest coil: 2 (or 3) , 2nd closest coil: 3 (or 2)

 - case 3 : z = C, eg. #5 < z 
   - closest coil: 5, 2nd closest coil: -1

 - case 4 : z = D, eg. z < #6 and #6 is closer than #5
   - closest coil: 1, 2nd closest coil: -1

 - case 5 : z = E, eg. #10 < z
   - closest coil: 10, 2nd closest coil: -1

*/
Int_t PaAlgo::GetSecondClosestNMRCoil( Int_t run, Double_t z )
{

  // this event should be taken in 2015
  assert( run > 256000 && run < 264860 );

  // get # of the closest coil
  Int_t closest_coil_num = GetClosestNMRCoil( run, z ) ;

  // special cases, see the explanation above
  // case of  z < #1
  if( closest_coil_num == 1       && z - GetZOfNMRCoil( run, closest_coil_num ) < 1e-5 )
    return -1;
  // case of  #5 < z
  else if( closest_coil_num ==  5 && GetZOfNMRCoil( run, closest_coil_num ) - z < 1e-5 )
    return -1;
  // case of  z < #6
  else if( closest_coil_num ==  6 && z - GetZOfNMRCoil( run, closest_coil_num )  < 1e-5 )
    return -1;
  // case of  #10 < z
  else if( closest_coil_num == 10 && GetZOfNMRCoil( run, closest_coil_num ) - z  < 1e-5 )
    return -1;

  // number of coil, should be 1 - 10
  Int_t coil_num = -1;

  if( z < GetZOfNMRCoil( run , closest_coil_num ) )
    coil_num = closest_coil_num - 1;
  else
    coil_num = closest_coil_num + 1;

  return coil_num;
}


/*!
  \brief return the polarization averaged with neighboring coils
  \param ev this event
  \param z  the position you want to see
  \details Polarization 

  User can specify a margin for target cell.
  Polarization outside of (target cell+margin) is 0.
  Polarization in margin of target is the same as the closest coil's polarization.
  See following figure

 \code
                   =====================               =====================
                  |                     |             |                     |
                   =====================               ===================== 
           |      |                     |      |      |                     |      |
           |      |                     |      |      |                     |      |
       --->|<---->|<------------------->|<---->|<---->|<------------------->|<---->|<---
 type:  out|margin|    upstream cell    |margin|margin|  downstream cell    |margin|out
 pol.:  0% | P(#1)|                     | P(#5)| P(#6)|                     |P(#10)| 0%
           |                                                                       |
 z(cm):   -315.0                                                                  -145.0
 \endcode

 */


/*!
  \brief return calculated pol.
  \param ev PaEvent class
  \param z  z coordinate where you want to calculate pol. at
  \details only pol. in 2015 is available
*/
Double_t PaAlgo::GetTargetPol( PaEvent& ev, Double_t z )
{

  Int_t run = ev.RunNum();

  assert( run > 256000 && run < 264860 );

  // assign of elements should be :
  // 08 - 12 : D_polar_coil{i} ( i=1,2,3,4,5),
  // 13 - 17 : D_polar_coil{i} (i=67,8,9,10) ,
  vector < float > vpol = PaMetaDB::Ref().Polarizations( run );

  // get z coordinate of target cells
  // naming : (up/down side)_(up/down cell)_z
  Double_t up_up_z, down_down_z, up_down_z, down_up_z, dtemp;
  Bool_t bl_target_location = GetTargetLocation( ev.RunNum(),
					       dtemp, dtemp, up_up_z  , down_up_z,
					       dtemp, dtemp, up_down_z, down_down_z, 
					       dtemp, dtemp );

  // define target region in z coordinate
  const Double_t kTarget_region_up   = -315.0;
  const Double_t kTarget_region_down = -145.0;

  // check if specified z is inside of (cell+margin) or not
  if( z < kTarget_region_up || z > kTarget_region_down )
    return 0.0;

  // indices should be from 1 to 10, or -1
  Int_t closest_coil_index        = PaAlgo::GetClosestNMRCoil( run , z );
  Int_t second_closest_coil_index = PaAlgo::GetSecondClosestNMRCoil( run , z );

  Double_t coil_z_1st = GetZOfNMRCoil( run, closest_coil_index );
  Double_t d_1st      = fabs( coil_z_1st - z );
  Double_t pol_1st    = vpol[ 7 + closest_coil_index ] ;

  // if the second closest coil is not defined, return the closest coil's polarization
  if( second_closest_coil_index == -1 )
    return pol_1st / 100.0 ;
  
  Double_t coil_z_2nd = GetZOfNMRCoil( run , second_closest_coil_index );
  Double_t d_2nd      = fabs( coil_z_2nd - z );
  Double_t d_total    = d_1st + d_2nd;
  Double_t pol_2nd    = vpol[ 7 + second_closest_coil_index ] ;

  return ( d_2nd * pol_1st + d_1st * pol_2nd ) / d_total / 100.0;
}
